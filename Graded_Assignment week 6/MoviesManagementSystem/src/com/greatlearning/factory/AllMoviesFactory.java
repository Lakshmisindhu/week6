package com.greatlearning.factory;

import com.greatlearning.dao.MoviesComing;
import com.greatlearning.dao.MoviesInTheaters;
import com.greatlearning.dao.TopRatingIndia;
import com.greatlearning.dao.TopRatingMovie;
import com.greatlearning.interfaces.AllMovies;

public class AllMoviesFactory {
	public static AllMovies createAllMovies(String channel) {
		
			if (channel == null || channel.isEmpty())
				return null;
			if ("moviescoming".equals(channel)) {
				return new MoviesComing();
			} else if ("moviesinTheater".equals(channel)) {
				return new MoviesInTheaters();
			} else if ("TopRatedIndia".equals(channel)) {
				return new TopRatingIndia();
			} else if ("topRatedMovies".equals(channel)) {
				return new TopRatingMovie();
			}

			return null;

		}
	
}
