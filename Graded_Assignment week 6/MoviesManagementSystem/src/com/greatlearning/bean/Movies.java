package com.greatlearning.bean;

public class Movies {
	int id;
	String title;
	int year;
	int Rating;
	String duration;
	String releaseDate;
	
	public Movies() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Movies(int id, String title, int year, int rating, String duration, String releaseDate) {
		super();
		this.id = id;
		this.title = title;
		this.year = year;
		Rating = rating;
		this.duration = duration;
		this.releaseDate = releaseDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getRating() {
		return Rating;
	}

	public void setRating(int rating) {
		Rating = rating;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	@Override
	public String toString() {
		return "Movies [id=" + id + ", title=" + title + ", year=" + year + ", Rating=" + Rating + ", duration="
				+ duration + ", releaseDate=" + releaseDate + "]";
	}
	
}
