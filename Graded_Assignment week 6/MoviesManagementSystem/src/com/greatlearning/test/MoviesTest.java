package com.greatlearning.test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.greatlearning.bean.Movies;

class MoviesTest {

	@Test
	public void testGetId() {
		//fail("Not yet implemented");
		Movies m1=new Movies();
		m1.setId(1);
		assertTrue(m1.getId()==1);
	}

	@Test
	public void testSetId() {
		//fail("Not yet implemented");
		Movies m1=new Movies();
		m1.setId(1);
		assertTrue(m1.getId()==1);
	}

	@Test
	public void testGetTitle() {
		//fail("Not yet implemented");
		Movies m1=new Movies();
		m1.setTitle("avathar");
		assertTrue(m1.getTitle()=="avathar");
	}

	@Test
	public void testSetTitle() {
		//fail("Not yet implemented");
		Movies m1=new Movies();
		m1.setTitle("avathar");
		assertTrue(m1.getTitle()=="avathar");
	}

	@Test
	public void testGetYear() {
		//fail("Not yet implemented");
		Movies m1=new Movies();
		m1.setYear(2010);
		assertTrue(m1.getYear()==2010);
	}

	@Test
	public void testSetYear() {
		//fail("Not yet implemented");
		Movies m1=new Movies();
		m1.setYear(2010);
		assertTrue(m1.getYear()==2010);
		
	}

}
