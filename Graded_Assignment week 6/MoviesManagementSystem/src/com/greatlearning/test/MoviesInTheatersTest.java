package com.greatlearning.test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.greatlearning.bean.Movies;
import com.greatlearning.dao.MoviesComing;
import com.greatlearning.dao.MoviesInTheaters;

class MoviesInTheatersTest {

	
	@Test
	public void testGetMovies() throws SQLException {
		//fail("Not yet implemented");
		MoviesInTheaters mc=new MoviesInTheaters();
		List<Movies> lo= mc.getMovies();
		
		assertEquals(10, lo.size());
	}

	@Test
	public void testInsert() throws SQLException {
		//fail("Not yet implemented");
		MoviesInTheaters mc=new MoviesInTheaters();
		Movies m=new Movies();
		m.setId(1);
		m.setTitle("narniya");
		int success=mc.insert(m);
		assertEquals(1,success);
	}

}
