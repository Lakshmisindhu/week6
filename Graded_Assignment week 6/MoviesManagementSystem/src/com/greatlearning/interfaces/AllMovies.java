package com.greatlearning.interfaces;

import java.sql.SQLException;
import java.util.List;

import com.greatlearning.bean.Movies;

public interface AllMovies 
{
	public List<Movies> getMovies() throws SQLException;

	public int insert(Movies movie) throws SQLException;
	 
}
