create database lakshmi;

use lakshmi

create table moviescoming(
id int(5) primary key,
title varchar(20),genres varchar(20),year int(4),rating int(2),releaseDate varchar(12));

insert into moviescoming values(1,'superman','action',2018,5,'2018-4-19');
insert into moviescoming values(2,'marval','romantic',2017,2,'2017-3-19');
insert into moviescoming values(3,'tiger','Drama',2012,3,'2012-4-13');
insert into moviescoming values(4,'spiderman','Thriller',2017,4,'2017-6-15');
insert into moviescoming values(5,'heaman','action',2017,2,'2017-4-13');

select * from moviescoming;

create table moviesintheater(
id int(5) primary key,
title varchar(20),genres varchar(20),year int(4),rating int(2),releaseDate varchar(12));

insert into moviesintheater values(1,'iron man','action',2017,4,'2017-4-17');
insert into moviesintheater values(2,'captian','Drama',2018,4,'2018-7-19');
insert into moviesintheater values(3,'dangal','action',2017,5,'2017-8-13');
insert into moviesintheater values(4,'jersey','romantic',2014,2,'2014-4-19');
insert into moviesintheater values(5,'harypotter','action',2012,7,'2012-6-17');

select * from moviesintheater;

create table topratingmovies(
id int(5) primary key,
title varchar(20),genres varchar(20),year int(4),rating int(2),releaseDate varchar(12));


insert into topratingmovies values(1,'iron man','action',2013,4,'2013-4-19');
insert into topratingmovies values(2,'apider man','Drama',2017,5,'2017-4-17');
insert into topratingmovies values(3,'spiderman','romantic',2014,4,'2014-4-17');
insert into topratingmovies values(4,'pk','Thriller',2017,3,'2017-4-19');
insert into topratingmovies values(5,'bommar','romantic',2018,6,'2018-4-18');

select * from topratingmovies;

create table topratingindian(
id int(5) primary key,
title varchar(20),genres varchar(20),year int(4),rating int(2),releaseDate varchar(12));

insert into topratingindian values(1,'roja','action',2017,4,'2017-7-19');
insert into topratingindian values(2,'good','romantic',2017,6,'2017-4-14');
insert into topratingindian values(3,'mayabazar','Drama',2017,5,'2017-3-16');
insert into topratingindian values(4,'lifeofpi','action',2017,4,'2017-4-12');
insert into topratingindian values(5,'sea','romantic',2017,3,'2017-7-17');
select * from topratingindian;
